let http = require("http");

let courses = [
	
	{
		name: "python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS",
		description: "Learn ExpressJS",
		price: 28000
	},

];

http.createServer(function(request,response){
	console.log(request.url);
	console.log(request.method);

if(request.url === "/" && request.method ==="GET"){
	response.writeHead(200,{'Content-Type':"text/plain"});
	response.end("Hello from our 4th Server. this is the endpoint");

} else if (request.url === "/" && request.method ==="POST"){
	response.writeHead(200,{'Content-Type':"text/plain"});
	response.end("This is our / endpoint. POST method request");

} else if(request.url === "/" && request.method ==="PUT"){
	response.writeHead(200,{'Content-Type':"text/plain"});
	response.end("This is our / endpoint. PUT method request");

} else if(request.url === "/" && request.method ==="DELETE"){
	response.writeHead(200,{'Content-Type':"text/plain"});
	response.end("This is our / endpoint. DELETE method request");
}

else if(request.url === "/courses" && request.method ==="GET"){
	response.writeHead(200,{'Content-Type':"application/json"});
	response.end(JSON.stringify(courses));
}
else if(request.url === "/courses" && request.method ==="POST"){

	let requestBody = "";

	request.on('data',function(data){
		//console.log(data);
		requestBody += data; //data stream is saved in the variable
	})

	//end step - this will run once after the request data has been completely sent from the client
	request.on('end',function(){
		//completely recieved data from the client and thus requestBody contains are the input
		console.log(requestBody);

		//update initial requestBody is in  JSON format. update to parsed format

		requestBody = JSON.parse(requestBody);
		// console.log(requestBody); //now an object
		
		courses.push(requestBody); //add the requestBody in the array
		//check the courses array if we are able to add our requestBody
		console.log(courses);

		response.writeHead(200,{'Content-Type':"application/json"});
		//send the updated courses array to the client as JSON format
		response.end(JSON.stringify(courses));

	})

}

}).listen(4000);
console.log("Server running at localhost:4000");